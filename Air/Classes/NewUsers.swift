//Name: Jay Patel

import UIKit

class NewUsers: NSObject {
    
    var id : Int?
    var FirstName : String?
    var LastName : String?
    var Email : String?
    var Password : String?
    var RePassword : String?
    var Contact : String?
    
    
    func initWithData(theRow i: Int, theFirstName F : String, theLastName L : String, theEmail E: String, thePassword P: String, theRePassword RP: String, theContact C : String)
    {
        id = i
        FirstName = F
        LastName = L
        Email = E
        Password = P
        RePassword = RP
        Contact = C
    }
}
