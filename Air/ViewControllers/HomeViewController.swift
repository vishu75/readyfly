//Name: Vishvakumar Mavani

import UIKit

class HomeViewController: UIViewController, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
    
    @IBOutlet var btnFlights : UIButton?
    @IBOutlet var txtDepartingDatePicker: UITextField!
    @IBOutlet var txtReturningDatePicker: UITextField!
    @IBOutlet var txtPreferredClass: UITextField!
    @IBOutlet var txtTravellers: UITextField!
    @IBOutlet var txtFlyingTo: UITextField!
    @IBOutlet var txtFlyingFrom: UITextField!
    @IBOutlet var stepper: UIStepper!
   
    let datePicker = UIDatePicker()
    let datePicker2 = UIDatePicker()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtReturningDatePicker.isUserInteractionEnabled = false
        showDatePicker()
        createPickerView()
    }
    
    var selectedClass: String?
    //an array for selecting flying class in pickerview
    var classList = ["First Class", "Business", "Premium Economy", "Economy"]
    
    var selectedCity: String?
    //an array for selecting flying city in pickerview
    var cityList = ["Toronto", "Delhi", "NewYork", "Mumbai", "Atlanta", "Ahmedabad"]
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1 // number of session
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if (txtPreferredClass.isEditing) {
            return classList.count // number of dropdown items in array flying class
        }else{
            return cityList.count // number of dropdown items in array city names
        }
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if (txtPreferredClass.isEditing) {
            return classList[row]
        }else{
            return cityList[row]
        }
    }
    
    //function for selecting an item from picker view.
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if (txtPreferredClass.isEditing) {
            selectedClass = classList[row] // selected item
            txtPreferredClass.text = selectedClass
        }
        if(txtFlyingFrom.isEditing){
            selectedCity = cityList[row]
            txtFlyingFrom.text = selectedCity
        }
        else{
            selectedCity = cityList[row]
            txtFlyingTo.text = selectedCity
        }
    }
    
    //creating two seperate picker views for displaying city names and flying classs
    func createPickerView() {
        
        //for flying class
        let pickerView = UIPickerView()
        pickerView.delegate = self
        
        let toolbar = UIToolbar();
          toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.endPickerAction));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)

        toolbar.setItems([doneButton,spaceButton], animated: false)
        
        txtPreferredClass.inputAccessoryView = toolbar
        txtPreferredClass.inputView = pickerView
        
        //for city names
        let cityPickerView = UIPickerView()
        cityPickerView.delegate = self
        
        let toolbar2 = UIToolbar();
          toolbar2.sizeToFit()
        let doneButton2 = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.endPickerAction));
        let spaceButton2 = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)

        toolbar2.setItems([doneButton2,spaceButton2], animated: false)
        
        txtFlyingFrom.inputAccessoryView = toolbar2
        txtFlyingFrom.inputView = cityPickerView
        
        txtFlyingTo.inputAccessoryView = toolbar2
        txtFlyingTo.inputView = cityPickerView
    }
    
    //for ending picker view
    @objc func endPickerAction() {
          view.endEditing(true)
    }
    
    //creating date picker view
    func showDatePicker(){
       //Formate departing date
        datePicker.datePickerMode = .date
        datePicker.minimumDate = NSDate() as Date

      //ToolBar for departing date
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));

        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)

        txtDepartingDatePicker.inputAccessoryView = toolbar
        txtDepartingDatePicker.inputView = datePicker
        
        //Formate returning Date
        datePicker2.datePickerMode = .date
        datePicker2.minimumDate = NSDate() as Date

        //ToolBar for returning date
        let toolbar2 = UIToolbar();
        toolbar2.sizeToFit()
        let doneButton2 = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker2));
        let spaceButton2 = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton2 = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));

        toolbar2.setItems([doneButton2,spaceButton2,cancelButton2], animated: false)
        
        txtReturningDatePicker.inputAccessoryView = toolbar2
        txtReturningDatePicker.inputView = datePicker2

    }
    
    //departingdate picker formatting to correct formate
    @objc func donedatePicker(){
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        txtDepartingDatePicker.text = formatter.string(from: datePicker.date)
        self.view.endEditing(true)
    }
    
    //returningdate picker formatting to correct formate
    @objc func donedatePicker2(){
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        txtReturningDatePicker.text = formatter.string(from: datePicker2.date)
        self.view.endEditing(true)
    }
    
    //end both date pickers
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
     }
    
//    @IBAction func stepperValueChanged(_ sender: UIStepper) {
//           txtTravellers.text = Int(sender.value).description
//    }
    
    //for selecting one way or round trip flights.
    @IBAction func segmentedControlButtonClickAction(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 1 {
        txtDepartingDatePicker.isUserInteractionEnabled = true
        txtReturningDatePicker.isUserInteractionEnabled = true
       }else{
        txtDepartingDatePicker.isUserInteractionEnabled = true
        txtReturningDatePicker.isUserInteractionEnabled = false
            txtReturningDatePicker.text = ""
       }
    }
    
    //checks that fields are not empty and then display flight accordingly.
    @IBAction func onSearch(sender : Any){
        if ((txtFlyingFrom.text?.isEmpty)! || (txtFlyingTo.text?.isEmpty)! || (txtDepartingDatePicker.text?.isEmpty)! || (txtReturningDatePicker.text?.isEmpty)! || (txtPreferredClass.text?.isEmpty)! || (txtTravellers.text?.isEmpty)!){
            
            // Display alert message here
            let alertController = UIAlertController(title: "Hello", message:
                           "Please Fill out every field", preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "TRY AGAIN !", style: .default))
                
                self.present(alertController, animated: true, completion: nil)
            
            return
        }else{
            self.performSegue(withIdentifier: "search", sender: self)
        }
    }
    
    //inserting bookings to a database as the user clicks comfirm button.
    @IBAction func insertBooking(sender : Any)
    {
        if ((txtFlyingFrom.text?.isEmpty)! || (txtFlyingTo.text?.isEmpty)! || (txtDepartingDatePicker.text?.isEmpty)! || (txtReturningDatePicker.text?.isEmpty)! || (txtPreferredClass.text?.isEmpty)! || (txtTravellers.text?.isEmpty)!){
            
            // Display alert message here
            let alertController = UIAlertController(title: "Hello", message:
                           "Please Fill out every field", preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "TRY AGAIN !", style: .default))
                
                self.present(alertController, animated: true, completion: nil)
            
            return
        }else{
            let bookings : Booking = Booking.init()
            bookings.initWithData(theRow: 0, thecountryfrom: txtFlyingFrom.text!, thecountryto: txtFlyingTo.text!, thedepartingdate: txtDepartingDatePicker.text!, thereturningdate: txtReturningDatePicker.text!, theflyclass: txtPreferredClass.text!, thetravellers: txtTravellers.text!)
            
            let mainDelegate = UIApplication.shared.delegate as! AppDelegate
            
            let returnCode : Bool = mainDelegate.insertIntoDatabase2(bookings: bookings)
            
            var returnMSG : String = "Booking done"
            
            if returnCode == false
            {
                returnMSG = "Booking Failed"
            }
            
            let alertController = UIAlertController(title: "SQLite Add", message: returnMSG, preferredStyle: .alert)
            
            let cancelAction = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
            
            alertController.addAction(cancelAction)
            present(alertController, animated: true)
        }
        
    }
    
    @IBAction func unwindToHomeVC(segue: UIStoryboardSegue) {
        
    }

}


