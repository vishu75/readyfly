//Name: Vishvakumar Mavani

import UIKit
import WebKit

class WebViewController: UIViewController {
    
    @IBOutlet var wbPage : WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let urlAddress = URL(string: "https://www.skyscanner.ca")
        let url = URLRequest(url: urlAddress!)
        wbPage?.load(url)
        
    }
    
    
  
}
